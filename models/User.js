var mongoose = require('mongoose');
const validator = require('validator');

var UserSchema = new mongoose.Schema({
    firstName : {
      type : String,
      required : true,
    },
    lastName : {
      type : String,
      required : true,
    },
    email : {
      type : String,
      required : true,
      minlength : 1,
      trim : true,
      unique : true,
      validate : {
        validator : validator.isEmail,
        message : '{VALUE} is not a valid email'
      }
    },
    password: {
      type : String,
      require : true,
      minlength : 6
    },
    mobile : {},
    designation : {
      type : String,
      require : true
    },
    address : {},
    tokens : [{
      access : {
        type : String,
        required : true
      },
      token : {
        type : String,
        required : true
      }
    }]
  });

  module.exports =mongoose.model('User',UserSchema);
