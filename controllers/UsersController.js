var mongoose = require("mongoose");
var User = require("../models/User");
var {ObjectId} = require('mongodb');
const _ = require('lodash');

var usersController = {};

// Create new user
usersController.create = function(req, res) {
  res.render("../views/users/create");
};

usersController.save = function(req,res){
  // console.log(req.body);
  var users = new User(req.body);
  users.save(function(err){
    if(err){
      console.log(err);
      res.render("../views/users/create");
    }else{
      console.log('Successfully created');
      res.redirect("/users");
    }
  });
};

// Show list of employees
usersController.listuser = function(req, res) {
  User.find().then((users) => {
    // console.log(users);
    res.render("../views/users/index", {users: users});
  },(e) => {
    console.log("Error:", err);
  });
};

usersController.showUser = function (req, res) {
  var id = req.params.id;
  if(!ObjectId.isValid(id)){
    console.log('invallid input id');
    res.send('Invallid id');
    return false;
  };
  User.findById(id).then((user) => {
    console.log('user listed successfully');
    res.render('../views/users/show',{user : user});
  },(e) => {
      console.log('Error',e);
  });
};

usersController.editUser = function(req,res){
  var id = req.params.id;
  if(!ObjectId.isValid(id)){
    console.log('not vallid id');
    res.end('not vallid id');
    return false;
  };
  User.findById(id).then((user) => {
    res.render('../views/users/edit',{ user : user});
  },(e) => {
    console.log('Error :',e);
  })
}

usersController.updateUser = function(req, res){
  var id = req.params.id;
  User.findByIdAndUpdate(id,
    // { $set: { firstName: req.body.firstName, mobile: req.body.mobile}},
    { $set: req.body},
    {new : true}).then((todo) => {
    console.log('success updated');
    res.redirect(`/users/show/${id}`);
  }).catch((e) =>{
    console.log('Error',e);
    res.send('Error');
  });
}

usersController.removeUser = function(req, res){
  var id = req.params.id;
  User.findByIdAndRemove(id).then((user) =>{
    console.log('User Deleted Successfully');
    res.redirect('/users/');
  },(e) =>{
    console.log('Error :',e);
    res.send('Error delating user.');
  });
};

module.exports = usersController;
