var express = require('express');
var router = express();
var users = require("../controllers/UsersController.js");
// console.log(users);

// Create user
router.get('/create', function(req, res) {
  users.create(req, res);
});

// Save users
router.post('/save', function(req, res) {
  users.save(req, res);
});

/* GET users listing. */
router.get('/', function(req, res) {
  users.listuser(req, res);
});

/* GET users listing. */
router.get('/show/:id', function(req, res) {
  users.showUser(req, res);
});

router.get('/edit/:id',(req,res) => {
  users.editUser(req, res);
});

router.post('/update/:id',(req, res) => {
  users.updateUser(req, res);
});

router.post('/delete/:id',(req, res) => {
  users.removeUser(req, res);
})

module.exports = router;
